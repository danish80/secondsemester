﻿
namespace GUI_App
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2GradientPanel1 = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.LogoutButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.StatusButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.PayButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.TaskPannel = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2GradientButton13 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton24 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.TaskButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.AttendenceButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.EmployeeInfoPannel = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.guna2GradientButton3 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton5 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton6 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton8 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.EmployeeInfoButton = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.guna2BorderlessForm2 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.guna2BorderlessForm3 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.guna2CustomGradientPanel1.SuspendLayout();
            this.guna2GradientPanel1.SuspendLayout();
            this.TaskPannel.SuspendLayout();
            this.EmployeeInfoPannel.SuspendLayout();
            this.guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(865, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 27);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(283, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(616, 430);
            this.panel1.TabIndex = 2;
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2GradientPanel1);
            this.guna2CustomGradientPanel1.Controls.Add(this.panel1);
            this.guna2CustomGradientPanel1.Controls.Add(this.pictureBox1);
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.AliceBlue;
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.MidnightBlue;
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.Aqua;
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.DarkGreen;
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(0, -2);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(911, 509);
            this.guna2CustomGradientPanel1.TabIndex = 0;
            this.guna2CustomGradientPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.guna2CustomGradientPanel1_Paint);
            // 
            // guna2GradientPanel1
            // 
            this.guna2GradientPanel1.AutoScroll = true;
            this.guna2GradientPanel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2GradientPanel1.BorderRadius = 5;
            this.guna2GradientPanel1.Controls.Add(this.LogoutButton);
            this.guna2GradientPanel1.Controls.Add(this.StatusButton);
            this.guna2GradientPanel1.Controls.Add(this.PayButton);
            this.guna2GradientPanel1.Controls.Add(this.TaskPannel);
            this.guna2GradientPanel1.Controls.Add(this.TaskButton);
            this.guna2GradientPanel1.Controls.Add(this.AttendenceButton);
            this.guna2GradientPanel1.Controls.Add(this.EmployeeInfoPannel);
            this.guna2GradientPanel1.Controls.Add(this.EmployeeInfoButton);
            this.guna2GradientPanel1.Controls.Add(this.guna2Panel1);
            this.guna2GradientPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2GradientPanel1.FillColor = System.Drawing.Color.Transparent;
            this.guna2GradientPanel1.FillColor2 = System.Drawing.Color.Transparent;
            this.guna2GradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.guna2GradientPanel1.Name = "guna2GradientPanel1";
            this.guna2GradientPanel1.Size = new System.Drawing.Size(291, 509);
            this.guna2GradientPanel1.TabIndex = 3;
            // 
            // LogoutButton
            // 
            this.LogoutButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.LogoutButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.LogoutButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.LogoutButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.LogoutButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.LogoutButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.LogoutButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.LogoutButton.FillColor2 = System.Drawing.Color.Ivory;
            this.LogoutButton.FocusedColor = System.Drawing.Color.Teal;
            this.LogoutButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.LogoutButton.ForeColor = System.Drawing.Color.Black;
            this.LogoutButton.Location = new System.Drawing.Point(0, 585);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(274, 38);
            this.LogoutButton.TabIndex = 41;
            this.LogoutButton.Text = "Log Out";
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // StatusButton
            // 
            this.StatusButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.StatusButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.StatusButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StatusButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StatusButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.StatusButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.StatusButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.StatusButton.FillColor2 = System.Drawing.Color.Ivory;
            this.StatusButton.FocusedColor = System.Drawing.Color.Teal;
            this.StatusButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.StatusButton.ForeColor = System.Drawing.Color.Black;
            this.StatusButton.Location = new System.Drawing.Point(0, 540);
            this.StatusButton.Name = "StatusButton";
            this.StatusButton.Size = new System.Drawing.Size(274, 45);
            this.StatusButton.TabIndex = 40;
            this.StatusButton.Text = "See Full Status";
            this.StatusButton.Click += new System.EventHandler(this.StatusButton_Click);
            // 
            // PayButton
            // 
            this.PayButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.PayButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.PayButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.PayButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.PayButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.PayButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.PayButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.PayButton.FillColor2 = System.Drawing.Color.Ivory;
            this.PayButton.FocusedColor = System.Drawing.Color.Teal;
            this.PayButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.PayButton.ForeColor = System.Drawing.Color.Black;
            this.PayButton.Location = new System.Drawing.Point(0, 502);
            this.PayButton.Name = "PayButton";
            this.PayButton.Size = new System.Drawing.Size(274, 38);
            this.PayButton.TabIndex = 39;
            this.PayButton.Text = "Borrow List";
            this.PayButton.Click += new System.EventHandler(this.PayButton_Click);
            // 
            // TaskPannel
            // 
            this.TaskPannel.Controls.Add(this.guna2GradientButton13);
            this.TaskPannel.Controls.Add(this.guna2GradientButton24);
            this.TaskPannel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TaskPannel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(188)))), ((int)(((byte)(213)))));
            this.TaskPannel.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.TaskPannel.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.TaskPannel.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.TaskPannel.Location = new System.Drawing.Point(0, 431);
            this.TaskPannel.Name = "TaskPannel";
            this.TaskPannel.Size = new System.Drawing.Size(274, 71);
            this.TaskPannel.TabIndex = 36;
            // 
            // guna2GradientButton13
            // 
            this.guna2GradientButton13.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton13.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton13.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton13.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton13.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton13.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton13.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton13.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton13.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton13.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton13.Location = new System.Drawing.Point(-3, 0);
            this.guna2GradientButton13.Name = "guna2GradientButton13";
            this.guna2GradientButton13.Size = new System.Drawing.Size(305, 36);
            this.guna2GradientButton13.TabIndex = 5;
            this.guna2GradientButton13.Text = "Change user Password";
            this.guna2GradientButton13.Click += new System.EventHandler(this.guna2GradientButton13_Click);
            // 
            // guna2GradientButton24
            // 
            this.guna2GradientButton24.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton24.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton24.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton24.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton24.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton24.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton24.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton24.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton24.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton24.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton24.Location = new System.Drawing.Point(0, 31);
            this.guna2GradientButton24.Name = "guna2GradientButton24";
            this.guna2GradientButton24.Size = new System.Drawing.Size(299, 41);
            this.guna2GradientButton24.TabIndex = 6;
            this.guna2GradientButton24.Text = "User List";
            this.guna2GradientButton24.Click += new System.EventHandler(this.guna2GradientButton24_Click);
            // 
            // TaskButton
            // 
            this.TaskButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.TaskButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.TaskButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.TaskButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.TaskButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.TaskButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.TaskButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.TaskButton.FillColor2 = System.Drawing.Color.Ivory;
            this.TaskButton.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.TaskButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.TaskButton.ForeColor = System.Drawing.Color.Black;
            this.TaskButton.Location = new System.Drawing.Point(0, 392);
            this.TaskButton.Name = "TaskButton";
            this.TaskButton.Size = new System.Drawing.Size(274, 39);
            this.TaskButton.TabIndex = 35;
            this.TaskButton.Text = "User Info...";
            this.TaskButton.Click += new System.EventHandler(this.TaskButton_Click);
            // 
            // AttendenceButton
            // 
            this.AttendenceButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AttendenceButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AttendenceButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AttendenceButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AttendenceButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AttendenceButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.AttendenceButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.AttendenceButton.FillColor2 = System.Drawing.Color.Ivory;
            this.AttendenceButton.FocusedColor = System.Drawing.Color.Teal;
            this.AttendenceButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.AttendenceButton.ForeColor = System.Drawing.Color.Black;
            this.AttendenceButton.Location = new System.Drawing.Point(0, 349);
            this.AttendenceButton.Name = "AttendenceButton";
            this.AttendenceButton.Size = new System.Drawing.Size(274, 43);
            this.AttendenceButton.TabIndex = 14;
            this.AttendenceButton.Text = "Change Password";
            this.AttendenceButton.Click += new System.EventHandler(this.AttendenceButton_Click);
            // 
            // EmployeeInfoPannel
            // 
            this.EmployeeInfoPannel.Controls.Add(this.guna2GradientButton3);
            this.EmployeeInfoPannel.Controls.Add(this.guna2GradientButton5);
            this.EmployeeInfoPannel.Controls.Add(this.guna2GradientButton6);
            this.EmployeeInfoPannel.Controls.Add(this.guna2GradientButton8);
            this.EmployeeInfoPannel.Dock = System.Windows.Forms.DockStyle.Top;
            this.EmployeeInfoPannel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(188)))), ((int)(((byte)(213)))));
            this.EmployeeInfoPannel.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.EmployeeInfoPannel.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.EmployeeInfoPannel.FillColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(241)))), ((int)(((byte)(253)))));
            this.EmployeeInfoPannel.Location = new System.Drawing.Point(0, 183);
            this.EmployeeInfoPannel.Name = "EmployeeInfoPannel";
            this.EmployeeInfoPannel.Size = new System.Drawing.Size(274, 166);
            this.EmployeeInfoPannel.TabIndex = 13;
            // 
            // guna2GradientButton3
            // 
            this.guna2GradientButton3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton3.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton3.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton3.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton3.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton3.Location = new System.Drawing.Point(0, 84);
            this.guna2GradientButton3.Name = "guna2GradientButton3";
            this.guna2GradientButton3.Size = new System.Drawing.Size(302, 35);
            this.guna2GradientButton3.TabIndex = 6;
            this.guna2GradientButton3.Text = "Update Books Data";
            this.guna2GradientButton3.Click += new System.EventHandler(this.guna2GradientButton3_Click);
            // 
            // guna2GradientButton5
            // 
            this.guna2GradientButton5.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton5.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton5.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton5.Font = new System.Drawing.Font("Segoe UI Semibold", 10.25F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton5.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton5.Location = new System.Drawing.Point(-2, 0);
            this.guna2GradientButton5.Name = "guna2GradientButton5";
            this.guna2GradientButton5.Size = new System.Drawing.Size(302, 42);
            this.guna2GradientButton5.TabIndex = 5;
            this.guna2GradientButton5.Text = "Add Books";
            this.guna2GradientButton5.Click += new System.EventHandler(this.guna2GradientButton5_Click);
            // 
            // guna2GradientButton6
            // 
            this.guna2GradientButton6.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton6.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton6.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton6.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton6.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton6.Font = new System.Drawing.Font("Segoe UI Semibold", 10.25F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton6.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton6.Location = new System.Drawing.Point(-3, 119);
            this.guna2GradientButton6.Name = "guna2GradientButton6";
            this.guna2GradientButton6.Size = new System.Drawing.Size(302, 47);
            this.guna2GradientButton6.TabIndex = 4;
            this.guna2GradientButton6.Text = "Remove Books";
            this.guna2GradientButton6.Click += new System.EventHandler(this.guna2GradientButton6_Click);
            // 
            // guna2GradientButton8
            // 
            this.guna2GradientButton8.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton8.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton8.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton8.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(248)))), ((int)(((byte)(240)))));
            this.guna2GradientButton8.FillColor2 = System.Drawing.Color.Silver;
            this.guna2GradientButton8.FocusedColor = System.Drawing.Color.PaleTurquoise;
            this.guna2GradientButton8.Font = new System.Drawing.Font("Segoe UI Semibold", 10.25F, System.Drawing.FontStyle.Bold);
            this.guna2GradientButton8.ForeColor = System.Drawing.Color.Black;
            this.guna2GradientButton8.Location = new System.Drawing.Point(0, 42);
            this.guna2GradientButton8.Name = "guna2GradientButton8";
            this.guna2GradientButton8.Size = new System.Drawing.Size(302, 42);
            this.guna2GradientButton8.TabIndex = 3;
            this.guna2GradientButton8.Text = "View Books";
            this.guna2GradientButton8.Click += new System.EventHandler(this.guna2GradientButton8_Click);
            // 
            // EmployeeInfoButton
            // 
            this.EmployeeInfoButton.AllowDrop = true;
            this.EmployeeInfoButton.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.EmployeeInfoButton.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.EmployeeInfoButton.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EmployeeInfoButton.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EmployeeInfoButton.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.EmployeeInfoButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.EmployeeInfoButton.FillColor = System.Drawing.Color.LightSeaGreen;
            this.EmployeeInfoButton.FillColor2 = System.Drawing.Color.Ivory;
            this.EmployeeInfoButton.FocusedColor = System.Drawing.Color.Teal;
            this.EmployeeInfoButton.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.EmployeeInfoButton.ForeColor = System.Drawing.Color.Black;
            this.EmployeeInfoButton.Location = new System.Drawing.Point(0, 137);
            this.EmployeeInfoButton.Name = "EmployeeInfoButton";
            this.EmployeeInfoButton.Size = new System.Drawing.Size(274, 46);
            this.EmployeeInfoButton.TabIndex = 12;
            this.EmployeeInfoButton.Text = "Books Info...";
            this.EmployeeInfoButton.Click += new System.EventHandler(this.EmployeeInfoButton_Click);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2Panel1.Controls.Add(this.guna2PictureBox2);
            this.guna2Panel1.Controls.Add(this.guna2PictureBox1);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(274, 137);
            this.guna2Panel1.TabIndex = 11;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.FillColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(93, 3);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(103, 94);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 1;
            this.guna2PictureBox1.TabStop = false;
            this.guna2PictureBox1.UseTransparentBackground = true;
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.AnimateWindow = true;
            this.guna2BorderlessForm1.BorderRadius = 50;
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // guna2BorderlessForm2
            // 
            this.guna2BorderlessForm2.AnimateWindow = true;
            this.guna2BorderlessForm2.BorderRadius = 25;
            this.guna2BorderlessForm2.ContainerControl = this;
            this.guna2BorderlessForm2.DockIndicatorTransparencyValue = 0.6D;
            this.guna2BorderlessForm2.TransparentWhileDrag = true;
            // 
            // guna2BorderlessForm3
            // 
            this.guna2BorderlessForm3.AnimateWindow = true;
            this.guna2BorderlessForm3.BorderRadius = 25;
            this.guna2BorderlessForm3.ContainerControl = this;
            this.guna2BorderlessForm3.DockIndicatorTransparencyValue = 0.6D;
            this.guna2BorderlessForm3.TransparentWhileDrag = true;
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.BorderRadius = 20;
            this.guna2PictureBox2.FillColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox2.Image")));
            this.guna2PictureBox2.ImageRotate = 0F;
            this.guna2PictureBox2.Location = new System.Drawing.Point(52, 14);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.Size = new System.Drawing.Size(177, 117);
            this.guna2PictureBox2.TabIndex = 2;
            this.guna2PictureBox2.TabStop = false;
            // 
            // guna2AnimateWindow1
            // 
            this.guna2AnimateWindow1.AnimationType = Guna.UI2.WinForms.Guna2AnimateWindow.AnimateWindowType.AW_SLIDE;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 505);
            this.Controls.Add(this.guna2CustomGradientPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.guna2GradientPanel1.ResumeLayout(false);
            this.TaskPannel.ResumeLayout(false);
            this.EmployeeInfoPannel.ResumeLayout(false);
            this.guna2Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private Guna.UI2.WinForms.Guna2GradientPanel guna2GradientPanel1;
        private Guna.UI2.WinForms.Guna2GradientButton LogoutButton;
        private Guna.UI2.WinForms.Guna2GradientButton StatusButton;
        private Guna.UI2.WinForms.Guna2GradientButton PayButton;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel TaskPannel;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton13;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton24;
        private Guna.UI2.WinForms.Guna2GradientButton TaskButton;
        private Guna.UI2.WinForms.Guna2GradientButton AttendenceButton;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel EmployeeInfoPannel;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton3;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton5;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton6;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton8;
        private Guna.UI2.WinForms.Guna2GradientButton EmployeeInfoButton;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm2;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm3;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
    }
}