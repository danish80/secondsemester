﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI_App
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            customizeDesign();
        }

        private void customizeDesign()
        {
            EmployeeInfoPannel.Visible = false;
            TaskPannel.Visible = false;
            //LeavePannel.Visible = false;
        }
        private void HideSubMenu()
        {
            if (EmployeeInfoPannel.Visible == true)
                EmployeeInfoPannel.Visible = false;
            if (TaskPannel.Visible == true)
                TaskPannel.Visible = false;
            //if (LeavePannel.Visible == true)
            //    LeavePannel.Visible = false;
        }
        private void ShowSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                HideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }

        }

        private void guna2CustomGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void EmployeeInfoButton_Click(object sender, EventArgs e)
        {
            ShowSubMenu(EmployeeInfoPannel);
        }

        private void guna2GradientButton5_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void guna2GradientButton8_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void guna2GradientButton3_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void guna2GradientButton6_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void AttendenceButton_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void TaskButton_Click(object sender, EventArgs e)
        {
            ShowSubMenu(TaskPannel);
        }

        private void guna2GradientButton13_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void guna2GradientButton24_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        //private void LeaveButton_Click(object sender, EventArgs e)
        //{
        //    ShowSubMenu(LeavePannel);
        //}

        private void guna2GradientButton28_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void guna2GradientButton27_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void PayButton_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void StatusButton_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You Will be Going To Sign In Page", "Message Box with Icon", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Form1 f = new Form1();
            f.Show();
            HideSubMenu();
        }

        private void LeaveButton_Click(object sender, EventArgs e)
        {

        }
    }
}
