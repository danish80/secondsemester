﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_App.BL;
using GUI_App.DL;
namespace GUI_App.Usercontrols
{
    public partial class register : UserControl
    {
        string path = "AdminData.txt";
        private static register instance;
        public static register Instance
        {
            get
            {
                if (instance == null)
                    instance = new register();
                return instance;
            }
        }
        public register()
        {
            InitializeComponent();
            txtPassword.UseSystemPasswordChar = true;
        }

        private void lblFooter_2_Click(object sender, EventArgs e)
        {
            if (CredentialsDL.getcount() != 0)
            {
                register.instance.Visible = false;
                register.instance.SendToBack();
                login.Instance.Visible = true;
                login.Instance.BringToFront();
            }
            else
            {
                MessageBox.Show("Create An Admin Account First");
            }

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

            if (validationForLogininEmty() == true)
            {
                Credentials s = takeinputsignup();
                CredentialsDL.storeDataInList(s);
                AdminDL.savedataAdmin(ref path,s);
                MessageBox.Show("Account Created");
                register.instance.Visible = false;
                register.instance.SendToBack();
                login.Instance.Visible = true;
                login.Instance.BringToFront();
            }


        }
        private Credentials takeinputsignup()
        {
            if (CredentialsDL.getcount() == 0)
            {
                string name = txtUsername.Text;
                string password = txtPassword.Text;
                txtPassword.UseSystemPasswordChar = true;
                string email = txtEmailAddress.Text;
                Credentials c = new Credentials(name, password, "admin");
                return c;
            }
            else
            {
                string name = txtUsername.Text;
                string password = txtPassword.Text;
                string email = txtEmailAddress.Text;
                Credentials c = new Credentials(name, password, "student");
                return c;
            }

        }
        private bool validationForLogininEmty() //   validation at login for empty input
        {
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()))
            {
                errorProvider1.SetError(txtUsername, "User name is required");
                return false;
            }
            else
            {
                errorProvider1.SetError(txtUsername, string.Empty);
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                errorProvider2.SetError(txtPassword, "Password is required");
                return false;
            }
            else
            {
                errorProvider2.SetError(txtPassword, string.Empty);
            }
            if (string.IsNullOrEmpty(txtEmailAddress.Text.Trim()))
            {
                errorProvider3.SetError(txtEmailAddress, "Password is required");
                return false;
            }
            else
            {
                errorProvider3.SetError(txtEmailAddress, string.Empty);
            }
            return true;
        }

        private void guna2ToggleSwitch1_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ToggleSwitch1.Checked == false)
            {
                txtPassword.UseSystemPasswordChar = true;
            }
            if (guna2ToggleSwitch1.Checked == true)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
        }
    }
}
