﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_App.BL;
using GUI_App.DL;

namespace GUI_App.Usercontrols
{
    public partial class login : UserControl
    {
        private static login instance;
        public static login Instance
        {
            get
            {
                if (instance == null)
                    instance = new login();
                return instance;
            }


        }
        public login()
        {
            InitializeComponent();
            txtPassword.UseSystemPasswordChar = true;
            
        }

        private void lblFooter_2_Click(object sender, EventArgs e)
        {
            Controls.Add(register.Instance);
            register.Instance.BringToFront();
            register.Instance.Visible = true;
        }

        private void login_Load(object sender, EventArgs e)
        {

        }
        private Credentials takeinputsignin()
        {
            string name = txtUsername.Text;
            string password = txtPassword.Text;
            txtPassword.UseSystemPasswordChar = true;
            Credentials c = new Credentials(name, password);
            return c;
        }
        private void guna2ToggleSwitch1_CheckedChanged(object sender, EventArgs e)
        {
            if(guna2ToggleSwitch1.Checked==false)
            {
                txtPassword.UseSystemPasswordChar = true;
            }
            if(guna2ToggleSwitch1.Checked==true)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
        
        }
        private bool validationForLogininEmty() //   validation at login for empty input
        {
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()))
            {
                errorProvider1.SetError(txtUsername, "User name is required");
                return false;
            }
            else
            {
                errorProvider1.SetError(txtUsername, string.Empty);
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                errorProvider2.SetError(txtPassword, "Password is required");
                return false;
            }
            else
            {
                errorProvider2.SetError(txtPassword, string.Empty);
            }
            return true;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (validationForLogininEmty() == true)
            {
                Credentials signIn = takeinputsignin();
                if (CredentialsDL.getcount() != 0)
                {
                    string ad = CredentialsDL.admincheck(signIn);
                    
                    if (ad == "admin")
                    {
                        MessageBox.Show("Admin Found");
                        this.Hide();
                        Form2 form = new Form2();
                        form.Show();
                    }
                    else if(ad=="student")
                    {
                        MessageBox.Show("Student found");
                    }
                    else
                    {
                        MessageBox.Show("no user found");
                    }
                }
                //if (UserDL.getList().Count != 0)
                //{
                //    EmployeeBL emp = EmployeeDL.SignIn(signIn);
                //    if (emp == null)
                //    {
                //        MessageBox.Show("Acount not found.");
                //    }
                //    else if (emp != null)
                //    {

                //        MessageBox.Show("User Login");
                //    }
                //}
            }
        }
    }
}
