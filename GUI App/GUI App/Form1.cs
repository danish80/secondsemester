﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_App.Usercontrols;
using GUI_App.BL;
using GUI_App.DL;
namespace GUI_App
{
    public partial class Form1 : Form
    {
        string path = "AdminData.txt";
        public Form1()
        {
            InitializeComponent();
            AdminDL.readData(ref path);
            MessageBox.Show(CredentialsDL.ad.Count.ToString());
            panel1.Controls.Add(login.Instance);
            if (CredentialsDL.getcount() != 0)
            {
                login.Instance.BringToFront();   // sign in comes in front
            }
            else
            {
                panel1.Controls.Add(register.Instance);
                register.Instance.BringToFront();
                login.Instance.Visible = false;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }
    }
}
