﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_App.BL
{
    class AdminBL:Credentials
    {
        private Credentials admin;
        public AdminBL()
        {

        }
        public AdminBL(Credentials s)
        {
            this.admin = s;
        }
        public AdminBL(string name, string password):base(name,password)
        {
            this.Name = name;
            this.Password = password;
        }
        public AdminBL(string name, string password,string role):base(name,password,role)
        {
            this.Name = name;
            this.Password = password;
            this.Role = role;
        }
    }
}
