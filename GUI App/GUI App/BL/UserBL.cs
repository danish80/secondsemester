﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_App.BL
{
    class UserBL:Credentials
    {
        public UserBL()
        {

        }
        public UserBL(string name, string password) : base(name, password)
        {
            this.Name = name;
            this.Password = password;
        }
        public UserBL(string name, string password, string role) : base(name, password, role)
        {
            this.Name = name;
            this.Password = password;
            this.Role = role;
        }
    }
}
