﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_App.BL
{
    class Credentials
    {

        private string name ;
        public string Name { get => name; set => name = value; }

        private string password ;
        public string Password { get => password; set => password = value; }
        private string role ;
        public string Role { get => role; set => role = value; }
        public  Credentials()
        { }
        public Credentials(string name, string password)
        {
            this.name = name;
            this.password = password;
        }
        public Credentials(string name, string password,string role)
        {
            this.name = name;
            this.password = password;
            this.role = role;
        }
    }
}
