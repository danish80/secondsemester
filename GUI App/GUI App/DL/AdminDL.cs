﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GUI_App.BL;

namespace GUI_App.DL
{
    class AdminDL
    {

        
        public static Credentials signIn(Credentials u)
        {
            foreach (Credentials storeduser in CredentialsDL.ad)
            {
                if (u.Name == storeduser.Name && u.Password == storeduser.Password)
                {
                    return storeduser;
                }
            }

            return null;
        }
        public static bool readData(ref string adpath)

        {
            if (File.Exists(adpath))
            {
                StreamReader fileVariable = new StreamReader(adpath);
                string record;
                while ((record = fileVariable.ReadLine()) != null)
                {
                    string[] temp = record.Split(',');
                    Credentials s = new Credentials(temp[0], temp[1], temp[2]);
                    
                    CredentialsDL.storeDataInList(s);
                }
                fileVariable.Close();
                return true;
            }
            return false;
        }

        public static void savedataAdmin(ref string path, Credentials ad)
        {
            StreamWriter file = new StreamWriter(path, true);

            file.WriteLine(ad.Name + "," + ad.Password + "," + ad.Role);
            file.Flush();
            file.Close();
        }
        
        
    }
}
