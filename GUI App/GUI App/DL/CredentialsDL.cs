﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI_App.BL;
namespace GUI_App.DL
{
    class CredentialsDL
    {
        public static List<Credentials> ad = new List<Credentials>();
        public static void storeDataInList(Credentials c)
        {
            ad.Add(c);
        }
        public static int getcount()
        {
            int num = ad.Count;
            return num;
        }
        public static string admincheck(Credentials u)
        {
            foreach (Credentials storeduser in ad)
            {

                if (u.Name == storeduser.Name && u.Password == storeduser.Password)
                {
                    return storeduser.Role;
                }
            }
            return null;
        }
    }
}
